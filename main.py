import json
from itertools import cycle, islice, dropwhile, product

strings = [u'E', u'A', u'D', u'G', u'B', u'E']
notes = [u'A', u'A#', u'B', u'C', u'C#', u'D', u'D#', u'E', u'F', u'F#', u'G', u'G#']
cycled_notes = cycle(notes)
string_to_note_index = {s: notes.index(s) for s in strings}
note_to_note_index = {s: notes.index(s) for s in notes}
fret_reach = 6
total_frets = 24

def parseNotes(str_notes):
	'''Given a string contains a sequence of notes, parse them in a list of notes.
	Ex: 'C, E, G' -> ['C', 'E', 'G']
	'''
	l_notes = str_notes.split(', ')
	return l_notes

def normalizeNotes(note):
	'''Given a note, return the corresponding sharp note if the note if flat.
	Ex: 'Bb' -> 'A#'
	    'A#' -> 'A#'
	'''
	if len(note) == 1:
		return note
	else:
		assert len(note) == 2
		if note[1] == '#':
			return note
		else:
			return notes[(note_to_note_index[note[0]] - 1) % 12]

def possible_notes(string, start_fret, chord_notes):
	'''Given a string, a start fret and some chord notes, generate a dict of possible fret: notes.
	Ex: ('E', 3, ['C', 'E', 'G']) -> {3: 'G', 8: 'C', 'O': 'E', 'X': None}
	'''
	start_note = notes[(string_to_note_index[string] + start_fret) % 12]
	skipped = dropwhile(lambda x: x != start_note, cycled_notes)
	reachable_notes = list(islice(skipped, None, fret_reach))
	relevant_reachable_notes = {(start_fret + i): n for i, n in enumerate(reachable_notes) if n in chord_notes}
	if string in chord_notes:
		relevant_reachable_notes['O'] = string
	relevant_reachable_notes['X'] = None
	return relevant_reachable_notes

def all_possible_combination(start_fret, chord_notes):
	'''Given a start fret and some chord notes, generate all possible combos to play that chord'''
	norm_chord_notes = map(normalizeNotes, chord_notes)
	possible_combination = []
	list_possible_notes = [possible_notes(s, start_fret, norm_chord_notes) for s in strings]
	for e in product(*list_possible_notes):
		played_notes = [list_possible_notes[i][e[i]] for i in xrange(len(strings))]
		if set(played_notes) - set([None]) == set(norm_chord_notes):
			possible_combination.append(e)
	return possible_combination


def write_notes(res):
	'''Writes output in required format.'''
	f = open('sol.txt', 'w')
	for chord_name, chord_notes in res.items():
		for chord_note in chord_notes:
			s = 'Chord:' + chord_name + '\n'
			for i, note in enumerate(chord_note):
				s += strings[i] + '---' + str(note) + '---\n'
			f.write(s)


def main():
	# read the chords data. list of dict with keys 'Name' and 'Notes'
	chords = json.load(open('chords.json', 'r'))
	res = {}
	l = 0
	print chords[0]
	stop()
	for chord in chords:
		chord_name = chord['Name']
		chord_notes = parseNotes(chord['Notes'])
		combos = []
		for start_fret in xrange(1, total_frets - fret_reach + 2):
			combos += all_possible_combination(start_fret, chord_notes)
		res[chord_name] = combos
		print '%d combos for chord %s' % (len(combos), chord_name)
		l += len(combos)
	print '%d total combos' % l
	write_notes(res)

if __name__ == '__main__':
	main()
